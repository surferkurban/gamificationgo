package endpoint

import (
	"bitbucket.org/surferkurban/gamificationgo/lib"
	"github.com/labstack/echo"
	"net/http"
	"os"
)

type PayLoadPost struct {
	Uuid  string `json:"uuid"`
	Type  string `json:"type"` //'numeric' 'thing'
	Value int64  `json:"value"`
}

var Config *config.Config

func PostData(c echo.Context) (err error) {
	Config = config.NewConfig(os.Getenv("GMF_MYSQL_CONNECTION"))
	body := new(PayLoadPost)

	if err = c.Bind(body); err != nil {
		return c.JSON(http.StatusBadRequest, "error:"+string(http.StatusBadRequest))
	}

	return c.JSON(http.StatusOK, body)
}
