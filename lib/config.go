package config

type Config struct {
	MysqlConnect string
}

func NewConfig(mysqlConnect string) *Config {
	return &Config{MysqlConnect: mysqlConnect}
}
